import datetime

from django.db.models import F
from django.contrib.auth import get_user_model
from rest_framework.reverse import reverse
from rest_framework.test import APITestCase
from rest_framework.test import APIRequestFactory, force_authenticate


from warehouse.models import Brand, Product, Order


User = get_user_model()


class OrderTestCase(APITestCase):
    def setUp(self):
        self.brand = Brand(name="Bayer")
        self.brand.save()

        self.product = Product(
            name="Ibuprofen",
            expiry_date=datetime.date.today() + datetime.timedelta(weeks=5),
            quantity=10,
            brand=self.brand,
        )
        self.product.save()
        
        self.user = User(
            first_name="John",
            last_name="Lennon",
            username="jlennon",
            email="jlennon@beatles.org"
        )
        self.user.set_password("pass123")
        self.user.save()

        self.client.login(username="jlennon", password="pass123")

    def tearDown(self) -> None:
        self.client.logout()
        return super().tearDown()

    def test_create_order(self):
        url = reverse("order_list")
        response = self.client.post(url, {
            "orderline_set": [
                {"product": self.product.pk, "quantity": 4}
            ]
        }, format="json")

        self.product.refresh_from_db()
        
        self.assertEqual(response.status_code, 201)
        self.assertEqual(self.product.quantity, 6)

    def test_cannot_order_more_than_available_quantity(self):
        url = reverse("order_list")
        response = self.client.post(url, {
            "orderline_set": [
                {"product": self.product.pk, "quantity": 14}
            ]
        }, format="json")

        self.product.refresh_from_db()
    
        self.assertEqual(response.status_code, 400)
        self.assertEqual(self.product.quantity, 10)

    def test_update_order(self):
        order = Order.objects.create(user=self.user)
        order.orderline_set.create(product=self.product, quantity=1)
        self.product.quantity -= 1
        self.product.save()

        url = reverse('order_details', kwargs={'pk': order.pk})
        
        response = self.client.patch(url, {"orderline_set": [
            {"product": self.product.pk, "quantity": 3}
        ]}, format="json")
        
        self.product.refresh_from_db()

        self.assertEqual(response.status_code, 200)
        self.assertEqual(self.product.quantity, 7)
