from django.db import models
from django.utils.translation import gettext_lazy as _

from users.models import User


class Brand(models.Model):
    class Meta:
        verbose_name = _('brand')

    name = models.CharField(_('name'), max_length=50)

    def __str__(self):
        return self.name


class Category(models.Model):
    class Meta:
        verbose_name = _('category')
        verbose_name_plural = 'categories'

    name = models.CharField(_('name'), max_length=30)

    def __str__(self):
        return self.name


class Product(models.Model):
    class Meta:
        verbose_name = _('product')

    name = models.CharField(_('name'), max_length=150)
    expiry_date = models.DateField(_('expiry date'))
    quantity = models.IntegerField(_('quantity'))
    brand = models.ForeignKey(Brand, verbose_name=_('brand'), on_delete=models.SET_NULL, null=True, blank=True)
    category = models.ForeignKey(Category, verbose_name=_('category'), on_delete=models.SET_NULL, null=True, blank=True)
    description = models.TextField(_('description'))
    created = models.DateTimeField(_('created at'), auto_now_add=True)
    modified = models.DateTimeField(_('modified at'), auto_now=True)

    def __str__(self):
        return self.name


class OrderLine(models.Model):
    class Meta:
        verbose_name = 'order_line'

    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    quantity = models.IntegerField()
    order = models.ForeignKey('Order', on_delete=models.CASCADE)

    def __str__(self):
        return self.product.name


class Order(models.Model):
    class Meta:
        verbose_name = 'order'

    user = models.ForeignKey(User, on_delete=models.CASCADE, blank=True)
    created = models.DateField(auto_now_add=True)
    updated = models.DateField(auto_now=True)

    def __str__(self):
        return self.user.get_full_name()
