from django.db import transaction
from django.contrib.auth import get_user_model
from django.utils.translation import gettext as _
from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from warehouse.models import Order, Product, OrderLine, Brand, Category

User = get_user_model()


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username')


class OrderLineProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = ('id', 'name')


class OrderLineSerializer(serializers.ModelSerializer):
    product = OrderLineProductSerializer

    class Meta:
        model = OrderLine
        fields = ['id', 'product', 'quantity']


class OrderDetailsSerializer(serializers.ModelSerializer):
    user = UserSerializer()
    orderline_set = OrderLineSerializer(many=True)

    class Meta:
        model = Order
        fields = ('id', 'user', 'created', 'orderline_set')
        read_only_fields = ("id", "user", "created")

    @transaction.atomic
    def update(self, instance, validated_data):
        order_lines_data = validated_data.pop('orderline_set')

        existing_quantities = {
            ol.product.pk: ol.quantity
            for ol in instance.orderline_set.all()
        }

        for order_line in order_lines_data:
            product = order_line['product']
            quantity = order_line["quantity"]

            if quantity > (product.quantity + existing_quantities.get(product.pk, 0)):
                raise ValidationError(_("Invalid quantity"))

            line, created = instance.orderline_set.update_or_create(product=product, defaults={"quantity": quantity})
            product.quantity = (product.quantity + existing_quantities.get(product.pk, 0)) - quantity
            product.save()

        return instance


class OrderListSerializer(serializers.ModelSerializer):
    orderline_set = OrderLineSerializer(many=True)

    class Meta:
        model = Order
        fields = ('id', 'user', 'created', 'orderline_set')
        read_only_fields = ("id", "user", "created")

    @transaction.atomic
    def create(self, validated_data):
        order_lines_data = validated_data.pop('orderline_set', [])
        user = self.context["request"].user
        order = Order.objects.create(user=user, **validated_data)

        for order_line in order_lines_data:
            product = order_line['product']
            if order_line["quantity"] > product.quantity:
                raise ValidationError(_("Invalid quantity"))
            product.quantity -= order_line["quantity"]
            product.save()
            order.orderline_set.create(**order_line)
        return order


class BrandSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=False)

    class Meta:
        model = Brand
        fields = ('id', 'name')
        read_only_fields = ['name']


class CategorySerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=False)

    class Meta:
        model = Category
        fields = ('id', 'name')
        read_only_fields = ['name']


class ProductReadSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = ["id", "name", "expiry_date",
                  "quantity", "brand", "category", "description",
                  "created", "modified"]

        depth = 1


class ProductWriteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = ['id', 'name', 'expiry_date',
                  'quantity', 'description',
                  'created', 'modified', 'brand', 'category']
