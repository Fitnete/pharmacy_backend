from django.contrib import admin

from warehouse.models import Brand, Category, Product, OrderLine, Order


@admin.register(Brand)
class BrandAdmin(admin.ModelAdmin):
    list_display = ("name",)


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = ("name",)


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    list_display = ("name", "expiry_date", "quantity", "brand", "category", "description")


@admin.register(Order)
class OrderLineAdmin(admin.ModelAdmin):
    list_display = ("user", "created", "updated")


@admin.register(OrderLine)
class OrderLineAdmin(admin.ModelAdmin):
    list_display = ("product", "quantity")
