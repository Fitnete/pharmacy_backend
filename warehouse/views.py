from rest_framework import generics
from django.utils.translation import gettext as _

from warehouse.models import Product, Order, OrderLine, Brand, Category
from warehouse.serializers import OrderListSerializer, OrderDetailsSerializer, BrandSerializer, \
    CategorySerializer, ProductReadSerializer, ProductWriteSerializer


class ProductListCreateView(generics.ListCreateAPIView):
    queryset = Product.objects.select_related("brand", "category").all()
    serializer_class = ProductReadSerializer

    def get_serializer_class(self):
        if self.request.method == 'POST':
            return ProductWriteSerializer
        return super().get_serializer_class()


class ProductDetails(generics.RetrieveUpdateDestroyAPIView):
    queryset = Product.objects.select_related("brand", "category").all()
    serializer_class = ProductReadSerializer

    def get_serializer_class(self):
        if self.request.method == 'PUT':
            return ProductWriteSerializer
        return super().get_serializer_class()


class OrderListView(generics.ListCreateAPIView):
    queryset = Order.objects.prefetch_related('orderline_set').all()
    serializer_class = OrderListSerializer

    def perform_create(self, serializer):
        return super().perform_create(serializer)


class OrderDetailsView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Order.objects.all()
    serializer_class = OrderDetailsSerializer


class BrandListView(generics.ListAPIView):
    queryset = Brand.objects.all()
    serializer_class = BrandSerializer


class BrandDetailsView(generics.RetrieveAPIView):
    queryset = Brand.objects.all()
    serializer_class = BrandSerializer


class CategoryListView(generics.ListAPIView):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer


class CategoryDetailsView(generics.RetrieveAPIView):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer
