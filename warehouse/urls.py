from django.urls import path

from warehouse import views


urlpatterns = [

    path('products/', views.ProductListCreateView.as_view(), name='products_list'),
    path('products/<int:pk>/', views.ProductDetails.as_view(), name='product_details'),
    path('brands/', views.BrandListView.as_view(), name='brands_list'),
    path('brands/<int:pk>/', views.BrandDetailsView.as_view(), name="brand_details"),
    path('categories/', views.CategoryListView.as_view(), name='categories_list'),
    path('categories/<int:id>/', views.CategoryDetailsView.as_view(), name='category_details'),
    path('orders/', views.OrderListView.as_view(), name='order_list'),
    path('orders/<int:pk>/', views.OrderDetailsView.as_view(), name='order_details'),


]
