
from django.contrib import admin
from django.urls import path, include


urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('warehouse.urls')),
    path('', include('pharmacy_backend.auth.urls')),
    path('', include('rest_framework.urls')),
]
