from django.contrib.auth import get_user_model, password_validation
from rest_framework import status
from rest_framework.exceptions import ValidationError
from rest_framework.generics import UpdateAPIView
from rest_framework.response import Response
from rest_framework_simplejwt.exceptions import TokenError, InvalidToken
from rest_framework_simplejwt.views import TokenObtainPairView
from django.core.exceptions import ValidationError as coreValidationError
from pharmacy_backend.auth.cons import ERROR_TYPE, OTHER, MESSAGE, ERRORS, FIELD_ERROR, VALIDATION_ERROR
from pharmacy_backend.auth.serializers import PharmacyTokenObtainPairSerializer, ChangePasswordSerializer
from pharmacy_backend.settings import AUTH_USER_MODEL
from users.models import User


class PharmacyTokenObtainPairView(TokenObtainPairView):
    """
    Takes a set of user credentials and returns an access and refresh JSON web
    token pair to prove the authentication of those credentials.
    """
    serializer_class = PharmacyTokenObtainPairSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)

        try:
            serializer.is_valid(raise_exception=True)
        except TokenError as e:
            raise InvalidToken(e.args[0])
        except ValidationError as e:
            return Response(
                {ERROR_TYPE: VALIDATION_ERROR, ERRORS: e.get_full_details(), MESSAGE: 'Wrong Credentials!'},
                status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            user = get_user_model().objects.filter(username=request.data['username']).first()
            if user:
                if not user.is_active:
                    return Response(
                        {ERROR_TYPE: OTHER, ERRORS: '{}'.format(e), MESSAGE: 'Not active user!'},
                        status=status.HTTP_400_BAD_REQUEST)
                else:
                    if not user.check_password(request.data['password']):
                        return Response({ERROR_TYPE: OTHER, ERRORS: '{}'.format(e),
                                         MESSAGE: 'Wrong password!'},
                                        status=status.HTTP_400_BAD_REQUEST)
                    else:
                        return Response(serializer.validated_data, status=status.HTTP_200_OK)
            else:
                return Response({ERROR_TYPE: OTHER, ERRORS: '{}'.format(e),
                                 MESSAGE: 'This user does not exist!'},
                                status=status.HTTP_400_BAD_REQUEST)
        return Response(serializer.validated_data, status=status.HTTP_200_OK)


class ChangePasswordView(UpdateAPIView):
    """
    API to change user's password.
    """
    serializer_class = ChangePasswordSerializer
    model = User

    def update(self, request, *args, **kwargs):
        user = self.request.user
        serializer = self.get_serializer(data=request.data)

        if serializer.is_valid():
            # Check old password
            if not user.check_password(serializer.data.get("old_password")):
                return Response({
                    ERROR_TYPE: FIELD_ERROR,
                    ERRORS: {'old_password': [{'message': 'Wrong old password.'}]},
                    MESSAGE: 'Wrong old password.'
                }, status=status.HTTP_400_BAD_REQUEST)

            new_password = serializer.data.get("new_password")
            try:
                password_validation.validate_password(password=new_password, user=AUTH_USER_MODEL)

            except coreValidationError as ve:
                return Response({
                    ERROR_TYPE: FIELD_ERROR,
                    ERRORS: {'new_password': [{'message': ve.messages[0]}]},
                    MESSAGE: ve.messages[0]}, status=status.HTTP_400_BAD_REQUEST)

            user.set_password(new_password)
            user.save()

            return Response({MESSAGE: 'Succesfully changed.'}, status=status.HTTP_200_OK)

        return Response({
            ERROR_TYPE: OTHER,
            ERRORS: serializer.errors,
            MESSAGE: 'Internal Problem.'
        }, status=status.HTTP_400_BAD_REQUEST)
