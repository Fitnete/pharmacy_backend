from django.urls import path
from rest_framework_simplejwt.views import TokenRefreshView

from pharmacy_backend.auth.views import PharmacyTokenObtainPairView, ChangePasswordView

urlpatterns = [
    path('api/token/', PharmacyTokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('api/token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
    path('auth/change-password/', ChangePasswordView.as_view(), name='change-password'),
]